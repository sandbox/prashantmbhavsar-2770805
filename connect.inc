<?php
/**
 * CRM development Credentials
 * Do not change below details
 */
define('CRM_SERVER', 'CRM_SERVER');
define('CRM_ORG_NAME', 'CRM_ORG_NAME');
define('CRM_WSDL', 'CRM_WSDL');
define('CRM_USERNAME', 'CRM_USERNAME');
define('CRM_PASSWORD', 'CRM_PASSWORD');