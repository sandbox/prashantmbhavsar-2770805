Drupal Microsoft CRM Connect module:
------------------------
Maintainers:
Prashant Manohar Bhavsar (https://www.drupal.org/user/2821841)
Requires - Drupal 7

Description
===========
Microsoft CRM Connect module is created to connect Microsoft Dynamic CRM to manage CRM data.
This module provides the mechanism to add/update/delete CRM data. 

This module is built around the NuSOAP - SOAP Toolkit for PHP using the Web Services provided by the Microsoft Dynamics CRM. It uses the adLDAP - LDAP Authentication with PHP for Active Directory if AD authentication is selected.

CRM field names mentioned in modules are specific to Microsoft Dynamic CRM. It may have different names.
Replace field names used in module with your CRM fields. Get connect to CRM administrator for exact names of fields. 

Installation
============
  1. Place the entire microsoft_crm_connect folder into your Drupal modules/ or better
     sites/x/modules/ directory.
  2. Enable the Microsoft CRM Connect module by navigating to
     Administer > Modules
  3. Utilise module functions to manage CRM